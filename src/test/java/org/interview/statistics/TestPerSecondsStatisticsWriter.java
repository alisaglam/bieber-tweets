package org.interview.statistics;

import java.util.HashMap;

import org.interview.common.repository.TwitterMessageRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class TestPerSecondsStatisticsWriter {

	private TwitterMessageRepository twitterMessageRepository;
	private StatisticsRepository statisticsRepository;
	
	@Before
	public void beforeTest() {
		twitterMessageRepository = Mockito.mock(TwitterMessageRepository.class);
		statisticsRepository = Mockito.mock(StatisticsRepository.class);
	}

	@Test
	public void testNullStatisticsRetrieved() {
		Mockito.when(twitterMessageRepository.getMessageCountGroupedByTime()).thenReturn(null);
		PerSecodsStatisticsWriter perSecodsStatisticsWriter = new PerSecodsStatisticsWriter(twitterMessageRepository, statisticsRepository);
		perSecodsStatisticsWriter.run();
		
		Mockito.verify(twitterMessageRepository, Mockito.times(1)).getMessageCountGroupedByTime();
		Mockito.verify(statisticsRepository, Mockito.times(1)).addPerSecondStatistics(null);
		Mockito.verifyNoMoreInteractions(twitterMessageRepository, statisticsRepository);
	}
	
	@Test
	@SuppressWarnings("serial")
	public void testNonNullStatisticsRetrieved() {
		HashMap<Long, Integer> statistics = new HashMap<Long, Integer>(){{put(1l, 3);put(2l,4);};};
		Mockito.when(twitterMessageRepository.getMessageCountGroupedByTime()).thenReturn(statistics);
		PerSecodsStatisticsWriter perSecodsStatisticsWriter = new PerSecodsStatisticsWriter(twitterMessageRepository, statisticsRepository);
		perSecodsStatisticsWriter.run();
		
		Mockito.verify(twitterMessageRepository, Mockito.times(1)).getMessageCountGroupedByTime();
		Mockito.verify(statisticsRepository, Mockito.times(1)).addPerSecondStatistics(statistics);
		Mockito.verifyNoMoreInteractions(twitterMessageRepository, statisticsRepository);
	}
}
