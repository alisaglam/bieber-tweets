package org.interview.statistics;

import java.util.HashMap;

import org.interview.common.file.FileWriter;
import org.interview.common.json.JsonException;
import org.interview.common.json.JsonObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class TestFileStatisticsRepository {

	private JsonObjectMapper jsonObjectMapperMock;
	private FileWriter fileWriterMock;
	
	@Before
	public void beforeTest() {
		jsonObjectMapperMock = Mockito.mock(JsonObjectMapper.class);
		fileWriterMock = Mockito.mock(FileWriter.class);
	}

	@Test
	public void testAddNullStatistics() throws JsonException {
		FileStatisticsRepository fileStatisticsRepository = new FileStatisticsRepository(jsonObjectMapperMock, fileWriterMock);
		fileStatisticsRepository.addPerSecondStatistics(null);
		
		Mockito.verify(jsonObjectMapperMock, Mockito.times(1)).serializeObjectToJson(null);
		Mockito.verify(fileWriterMock, Mockito.times(1)).writeStringToFile(Mockito.anyString(), Mockito.anyString());
	}
	
	@Test
	@SuppressWarnings("serial")
	public void testAddPerSecondStatistics() throws JsonException {
		FileStatisticsRepository fileStatisticsRepository = new FileStatisticsRepository(jsonObjectMapperMock, fileWriterMock);
		HashMap<Long, Integer> statistics = new HashMap<Long, Integer>(){{put(1l, 3);put(2l,4);};};
		fileStatisticsRepository.addPerSecondStatistics(statistics);
		
		Mockito.verify(jsonObjectMapperMock, Mockito.times(1)).serializeObjectToJson(statistics);
		Mockito.verify(fileWriterMock, Mockito.times(1)).writeStringToFile(Mockito.anyString(), Mockito.anyString());
	}
}
