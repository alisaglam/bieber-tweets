package org.interview.common.json;

import static org.junit.Assert.assertEquals;

import org.interview.common.entity.Author;
import org.interview.common.entity.Message;
import org.junit.Test;

public class TestTwitterMessageSerializer {

	@Test
	public void test() throws JsonException {
		Message message = new Message(10l, 150l, "alis", new Author(1l, 7l, "alisaglam", "alis"));
		String result = new JsonObjectMapperImpl().serializeObjectToJson(message);
		assertEquals("{\"id\":10,\"creationTimeInEpochSecods\":150,\"text\":\"alis\"}", result);
	}
}
