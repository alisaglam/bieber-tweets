package org.interview.common.json;

import org.interview.common.entity.Author;
import org.junit.Test;

public class TestJacksonObjectMapperImpl {

	@Test(expected = JsonException.class)
	public void testWithNullJson() throws JsonException {
		JsonObjectMapperImpl jsonObjectMapperImpl = new JsonObjectMapperImpl();
		jsonObjectMapperImpl.deserializeJson("sdsad", Author.class);
	}
}
