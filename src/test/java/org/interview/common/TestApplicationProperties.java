package org.interview.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestApplicationProperties {

	@Test
	public void testLoadProperties() {
		ApplicationProperties applicationProperties = ApplicationProperties.getInstance();
		String consumerKey = applicationProperties.getPropery(ApplicationProperties.CONSUMER_KEY);
		assertEquals("RLSrphihyR4G2UxvA0XBkLAdl", consumerKey);
		String consumerSecret = applicationProperties.getPropery(ApplicationProperties.CONSUMER_SECRET);
		assertEquals("FTz2KcP1y3pcLw0XXMX5Jy3GTobqUweITIFy4QefullmpPnKm4", consumerSecret);
		String phrase = applicationProperties.getPropery(ApplicationProperties.PHRASE);
		assertEquals("bieber", phrase);
	}
}
