package org.interview.common.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestTwitterEntity {

	@Test
	public void testHashcode() {
		Author author = new Author();
		author.setId(10l);
		Author author2 = new Author();
		assertNotEquals(author.hashCode(), author2.hashCode());
		author2.setId(10l);
		assertEquals(author.hashCode(), author2.hashCode());
	}
	
	public void testEquals() {
		Message message = new Message();
		message.setId(1199);
		Message message2 = new Message();
		assertFalse(message2.equals(message));
		message2.setId(1199);
		assertTrue(message2.equals(message));
	}
	
	public void testSort() {
		Message message = new Message();
		message.setCreationTimeInEpochSecods(2);
		Message message2 = new Message();
		assertFalse(message2.equals(message));
		message2.setCreationTimeInEpochSecods(1);
		assertTrue(message.compareTo(message2) > 0);
		message2.setCreationTimeInEpochSecods(2);
		assertTrue(message.compareTo(message2) == 0);
		message2.setCreationTimeInEpochSecods(3);
		assertTrue(message.compareTo(message2) < 0);
	}
}
