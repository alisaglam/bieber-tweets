package org.interview.common.queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class TestInMemoryTwitterMessageQueue {

	@Test
	public void testSendAndPoll() {
		TwitterMessageQueue<String> simpleInMemoryMessageConsumeQueue = new InMemoryTwitterMessageQueue<String>();
		
		String message = simpleInMemoryMessageConsumeQueue.poll(0);
		assertNull(message);
		simpleInMemoryMessageConsumeQueue.send("test_alis");
		message = simpleInMemoryMessageConsumeQueue.poll(0);
		assertEquals("test_alis",  message);
	}
}
