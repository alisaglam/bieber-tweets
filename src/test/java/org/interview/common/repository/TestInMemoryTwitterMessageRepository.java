package org.interview.common.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.interview.common.entity.Author;
import org.interview.common.entity.Message;
import org.junit.Before;
import org.junit.Test;

public class TestInMemoryTwitterMessageRepository {
	

	private InMemoryTwitterMessageRepository inMemoryTwitterMessageRepository;

	@Before
	public void getRepositoryContainingTestData() {
		inMemoryTwitterMessageRepository = new InMemoryTwitterMessageRepository();
		Author author1 = new Author(1, 1, "a", "b");
		Author author2 = new Author(2, 2, "c", "d");
		Message message = new Message(1l, 1l, "message1", author1);
		Message message2 = new Message(2l, 1l, "message2", author1);
		Message message3 = new Message(3l, 1l, "message3", author2);
		inMemoryTwitterMessageRepository.addMessage(message);
		inMemoryTwitterMessageRepository.addMessage(message2);
		inMemoryTwitterMessageRepository.addMessage(message3);
	}

	@Test
	public void testGetDistinctAuthorsInChronologicallyAscOrder() {
		List<Author> authors = inMemoryTwitterMessageRepository.getDistinctAuthorsInChronologicallyAscOrder();
		assertEquals(2, authors.size());
		assertEquals(1, authors.get(0).getId());
		assertEquals(2, authors.get(1).getId());
	}
	
	@Test
	public void testGetChronologicallySortedMessagesOfTheAuthor() {
		List<Message> messagesOfTheAuthor = inMemoryTwitterMessageRepository.getChronologicallySortedMessagesOfTheAuthor(new Author(1, 0, null, null));
		
		assertEquals(2, messagesOfTheAuthor.size());
		assertEquals(1, messagesOfTheAuthor.get(0).getId());
		assertEquals(2, messagesOfTheAuthor.get(1).getId());
	}
	
	@Test
	public void testGetChronologicallySortedMessagesWithNullAuthor() {
		List<Message> messagesOfTheAuthor = inMemoryTwitterMessageRepository.getChronologicallySortedMessagesOfTheAuthor(null);
		assertTrue(messagesOfTheAuthor.isEmpty());
	}
	
	@Test
	public void testMessageCountGroupedByTime() {
		Map<Long, Integer> messageCountGroupedByTime = inMemoryTwitterMessageRepository.getMessageCountGroupedByTime();
		assertEquals(1, messageCountGroupedByTime.keySet().size());
		assertEquals(3, messageCountGroupedByTime.get(1l).intValue());
	}
}
