package org.interview.writer;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.interview.common.entity.Author;
import org.interview.common.entity.Message;
import org.junit.Test;

public class TestGroupedMessage {

	@Test
	public void testSorting() {
		List<Message> messages = Arrays.asList(new Message(77, 4, null, null), new Message(88, 2, null, null));
		Author author = new Author(1, 3, null, null);
		GroupedMessage groupedMessage = new GroupedMessage(author, messages);
		GroupedMessage groupedMessage2 = new GroupedMessage(new Author(1, 1, null, null), messages);
		assertTrue(groupedMessage.compareTo(groupedMessage2) > 0);
	}
}
