package org.interview.writer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.interview.common.entity.Author;
import org.interview.common.entity.Message;
import org.interview.common.json.JsonException;
import org.interview.common.json.JsonObjectMapper;
import org.interview.common.repository.TwitterMessageRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class TestTwitterMessageWriter {

	private JsonObjectMapper jsonObjectMapper;
	private TwitterMessageRepository twitterMessageRepository;
	private OutputProvider outputProvider;
	
	@Before
	public void beforeTest() {
		jsonObjectMapper = Mockito.mock(JsonObjectMapper.class);
		twitterMessageRepository = Mockito.mock(TwitterMessageRepository.class);
		outputProvider = Mockito.mock(OutputProvider.class);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testNoMessage() throws JsonException {
		Mockito.when(twitterMessageRepository.getDistinctAuthorsInChronologicallyAscOrder()).thenReturn(Collections.EMPTY_LIST);
		
		TwitterMessageWriter twitterMessageWriter = new TwitterMessageWriter(jsonObjectMapper, twitterMessageRepository, outputProvider);
		twitterMessageWriter.run();
		
		Mockito.verify(twitterMessageRepository, Mockito.times(1)).getDistinctAuthorsInChronologicallyAscOrder();
		Mockito.verify(jsonObjectMapper, Mockito.times(1)).serializeObjectToJson(Collections.EMPTY_LIST);
		Mockito.verifyZeroInteractions(jsonObjectMapper, twitterMessageRepository, outputProvider);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testAuthorWithNoMessages() throws JsonException {
		Author author = new Author(10, 1, null, null);
		Mockito.when(twitterMessageRepository.getDistinctAuthorsInChronologicallyAscOrder()).thenReturn(Arrays.asList(author));
		Mockito.when(twitterMessageRepository.getChronologicallySortedMessagesOfTheAuthor(author)).thenReturn(Collections.EMPTY_LIST);
		Mockito.when(jsonObjectMapper.serializeObjectToJson(Mockito.any())).thenReturn("testjson");
		
		TwitterMessageWriter twitterMessageWriter = new TwitterMessageWriter(jsonObjectMapper, twitterMessageRepository, outputProvider);
		twitterMessageWriter.run();
	
		Mockito.verify(twitterMessageRepository, Mockito.times(1)).getDistinctAuthorsInChronologicallyAscOrder();
		Mockito.verify(twitterMessageRepository, Mockito.times(1)).getChronologicallySortedMessagesOfTheAuthor(author);
		Mockito.verify(jsonObjectMapper, Mockito.times(1)).serializeObjectToJson(Arrays.asList(new GroupedMessage(author, Collections.EMPTY_LIST)));
		Mockito.verify(outputProvider, Mockito.times(1)).provideOutput(Mockito.anyString());
		Mockito.verifyZeroInteractions(jsonObjectMapper, twitterMessageRepository, outputProvider);
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void testAuthorWithMessageList() throws JsonException {
		Author author = new Author(10, 1, null, null);
		Mockito.when(twitterMessageRepository.getDistinctAuthorsInChronologicallyAscOrder()).thenReturn(Arrays.asList(author));
		List<Message> messages = Arrays.asList(new Message(1, 0, null, null));
		Mockito.when(twitterMessageRepository.getChronologicallySortedMessagesOfTheAuthor(author)).thenReturn(messages);
		Mockito.when(jsonObjectMapper.serializeObjectToJson(Mockito.any())).thenReturn("testjson");
		
		TwitterMessageWriter twitterMessageWriter = new TwitterMessageWriter(jsonObjectMapper, twitterMessageRepository, outputProvider);
		twitterMessageWriter.run();
	
		Mockito.verify(twitterMessageRepository, Mockito.times(1)).getDistinctAuthorsInChronologicallyAscOrder();
		Mockito.verify(twitterMessageRepository, Mockito.times(1)).getChronologicallySortedMessagesOfTheAuthor(author);
		Mockito.verify(jsonObjectMapper, Mockito.times(1)).serializeObjectToJson(Arrays.asList(new GroupedMessage(author, Collections.EMPTY_LIST)));
		Mockito.verify(outputProvider, Mockito.times(1)).provideOutput(Mockito.anyString());
		Mockito.verifyZeroInteractions(jsonObjectMapper, twitterMessageRepository, outputProvider);
	}
}
