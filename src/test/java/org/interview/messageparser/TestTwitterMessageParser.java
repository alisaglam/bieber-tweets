package org.interview.messageparser;

import static org.junit.Assert.assertEquals;

import org.interview.common.entity.Message;
import org.interview.common.json.JsonException;
import org.interview.common.json.JsonObjectMapper;
import org.interview.common.queue.TwitterMessageQueue;
import org.interview.common.repository.TwitterMessageRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

public class TestTwitterMessageParser {
	private JsonObjectMapper jsonObjectMapper;
	private TwitterMessageQueue<String> messageConsumeQueueMock;
	private TwitterMessageRepository twitterMessageRepository;

	@Before
	@SuppressWarnings("unchecked")
	public void beforeTest() {
		messageConsumeQueueMock = Mockito.mock(TwitterMessageQueue.class);
		jsonObjectMapper = Mockito.mock(JsonObjectMapper.class);
		twitterMessageRepository = Mockito.mock(TwitterMessageRepository.class);
	}
	
	@Test
	public void testNoMessageRetrieved() {
		Mockito.when(messageConsumeQueueMock.poll(0)).thenReturn(TwitterMessageQueue.NO_MORE_MESSAGE);
		
		TwitterMessageParser twitterMessageParser = new TwitterMessageParser(messageConsumeQueueMock, jsonObjectMapper, twitterMessageRepository);
		twitterMessageParser.run();
		
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).poll(0);
		Mockito.verifyNoMoreInteractions(messageConsumeQueueMock);
	}

	@Test
	public void testOneMessageRetrieved() throws JsonException {
		Mockito.when(messageConsumeQueueMock.poll(0)).thenReturn("json_test");
		Mockito.when(messageConsumeQueueMock.poll(1)).thenReturn(TwitterMessageQueue.NO_MORE_MESSAGE);
		Mockito.when(jsonObjectMapper.deserializeJson("json_test", Message.class)).thenReturn(new Message(174l, 0l, null, null));
		
		
		TwitterMessageParser twitterMessageParser = new TwitterMessageParser(messageConsumeQueueMock, jsonObjectMapper, twitterMessageRepository);
		twitterMessageParser.run();
		
		ArgumentCaptor<Message> argument = ArgumentCaptor.forClass(Message.class);
		Mockito.verify(twitterMessageRepository, Mockito.times(1)).addMessage(argument.capture());
		assertEquals(174l, argument.getValue().getId());
		
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).poll(0);
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).poll(1);
		Mockito.verify(jsonObjectMapper, Mockito.times(1)).deserializeJson("json_test", Message.class);
		Mockito.verifyNoMoreInteractions(messageConsumeQueueMock, jsonObjectMapper);
	}

}
