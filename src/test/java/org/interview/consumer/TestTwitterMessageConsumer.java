package org.interview.consumer;

import org.interview.common.queue.TwitterMessageQueue;
import org.interview.consumer.track.TwitterApiCaller;
import org.interview.consumer.track.TwitterMessageReader;
import org.junit.Test;
import org.mockito.Mockito;

public class TestTwitterMessageConsumer {

	private TwitterApiCaller twitterMessageRetrieverMock;
	private TwitterMessageQueue<String> messageConsumeQueueMock;
	private TwitterMessageReader twitterMessageReaderMock;
	
	@SuppressWarnings("unchecked")
	private void initializeMocks() {
		messageConsumeQueueMock = Mockito.mock(TwitterMessageQueue.class);
		twitterMessageRetrieverMock = Mockito.mock(TwitterApiCaller.class);
		twitterMessageReaderMock = Mockito.mock(TwitterMessageReader.class);

		Mockito.when(twitterMessageRetrieverMock.getMessagesFromTwitter()).thenReturn(twitterMessageReaderMock);
		Mockito.when(twitterMessageReaderMock.readMessage()).thenReturn("test_alis");

	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void testTwitterMessageReaderReturnsNull() {
		messageConsumeQueueMock = Mockito.mock(TwitterMessageQueue.class);
		twitterMessageRetrieverMock = Mockito.mock(TwitterApiCaller.class);
		Mockito.when(twitterMessageRetrieverMock.getMessagesFromTwitter()).thenReturn(null);
		
		TwitterMessageConsumer twitterMessageConsumer = new TwitterMessageConsumer(messageConsumeQueueMock, twitterMessageRetrieverMock, 3, 3);
		twitterMessageConsumer.run();
		
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).send(TwitterMessageQueue.NO_MORE_MESSAGE);
		Mockito.verifyNoMoreInteractions(messageConsumeQueueMock);
	}

	@Test
	public void testMaxMessageSizeZero() {
		initializeMocks();
		TwitterMessageConsumer twitterMessageConsumer = new TwitterMessageConsumer(messageConsumeQueueMock, twitterMessageRetrieverMock, 0, 100000);
		twitterMessageConsumer.run();
		
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).send(TwitterMessageQueue.NO_MORE_MESSAGE);
		Mockito.verifyNoMoreInteractions(messageConsumeQueueMock);
	}
	
	@Test
	public void testMaxMessageSizeOne() {
		initializeMocks();
		TwitterMessageConsumer twitterMessageConsumer = new TwitterMessageConsumer(messageConsumeQueueMock, twitterMessageRetrieverMock, 1, 100000);
		twitterMessageConsumer.run();
		
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).send("test_alis");
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).send(TwitterMessageQueue.NO_MORE_MESSAGE);
		Mockito.verifyNoMoreInteractions(messageConsumeQueueMock);
	}
	
	@Test
	public void testZeroDurationInMilis() {
		initializeMocks();
		TwitterMessageConsumer twitterMessageConsumer = new TwitterMessageConsumer(messageConsumeQueueMock, twitterMessageRetrieverMock, 100000, 0);
		twitterMessageConsumer.run();
		
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).send(TwitterMessageQueue.NO_MORE_MESSAGE);
		Mockito.verifyNoMoreInteractions(messageConsumeQueueMock);
	}
	
	@Test
	public void testNonZeroDuration() {
		initializeMocks();
		TwitterMessageConsumer twitterMessageConsumer = new TwitterMessageConsumer(messageConsumeQueueMock, twitterMessageRetrieverMock, 15, 10);
		twitterMessageConsumer.run();
		
		Mockito.verify(messageConsumeQueueMock, Mockito.atLeastOnce()).send("test_alis");
		Mockito.verify(messageConsumeQueueMock, Mockito.atMost(15)).send("test_alis");
		Mockito.verify(messageConsumeQueueMock, Mockito.times(1)).send(TwitterMessageQueue.NO_MORE_MESSAGE);
		Mockito.verifyNoMoreInteractions(messageConsumeQueueMock);
	}
}
