package org.interview.consumer.track;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;

import org.interview.common.http.TwitterAuthenticatedHttpMethodCaller;
import org.interview.common.http.TwitterHttpRequest;
import org.interview.common.http.TwitterHttpResponse;
import org.junit.Test;
import org.mockito.Mockito;

public class TestTwitterTrackApiCaller {

	@Test
	public void testNullResponse() throws IOException {
		TwitterAuthenticatedHttpMethodCaller twitterAuthenticatedHttpMethodCallerMock = Mockito.mock(TwitterAuthenticatedHttpMethodCaller.class);
		TwitterHttpRequest twitterHttpRequestMock = Mockito.mock(TwitterHttpRequest.class);
		
		Mockito.when(twitterAuthenticatedHttpMethodCallerMock.buildGetRequest(Mockito.anyString())).thenReturn(twitterHttpRequestMock);
		Mockito.when(twitterHttpRequestMock.execute()).thenReturn(null);
		
		TwitterTrackApiCaller twitterTrackApiMessageRetrieverImpl = new TwitterTrackApiCaller(twitterAuthenticatedHttpMethodCallerMock);
		TwitterMessageReader reader = twitterTrackApiMessageRetrieverImpl.getMessagesFromTwitter();
		assertNull(reader);
	}
	
	@Test
	public void testContentOfTheResponseIsNull() throws IOException {
		TwitterAuthenticatedHttpMethodCaller twitterAuthenticatedHttpMethodCallerMock = Mockito.mock(TwitterAuthenticatedHttpMethodCaller.class);
		TwitterHttpRequest twitterHttpRequestMock = Mockito.mock(TwitterHttpRequest.class);
		TwitterHttpResponse twitterHttpResponseMock = Mockito.mock(TwitterHttpResponse.class);
		
		Mockito.when(twitterAuthenticatedHttpMethodCallerMock.buildGetRequest(Mockito.anyString())).thenReturn(twitterHttpRequestMock);
		Mockito.when(twitterHttpRequestMock.execute()).thenReturn(twitterHttpResponseMock);
		Mockito.when(twitterHttpResponseMock.getContent()).thenReturn(null);
		
		TwitterTrackApiCaller twitterTrackApiMessageRetrieverImpl = new TwitterTrackApiCaller(twitterAuthenticatedHttpMethodCallerMock);
		TwitterMessageReader reader = twitterTrackApiMessageRetrieverImpl.getMessagesFromTwitter();
		assertNull(reader);
	}
	
	@Test
	public void testReturnNotNullResponse() throws IOException {
		TwitterAuthenticatedHttpMethodCaller twitterAuthenticatedHttpMethodCallerMock = Mockito.mock(TwitterAuthenticatedHttpMethodCaller.class);
		TwitterHttpRequest twitterHttpRequestMock = Mockito.mock(TwitterHttpRequest.class);
		TwitterHttpResponse twitterHttpResponseMock = Mockito.mock(TwitterHttpResponse.class);
		
		Mockito.when(twitterAuthenticatedHttpMethodCallerMock.buildGetRequest(Mockito.anyString())).thenReturn(twitterHttpRequestMock);
		Mockito.when(twitterHttpRequestMock.execute()).thenReturn(twitterHttpResponseMock);
		Mockito.when(twitterHttpResponseMock.getContent()).thenReturn(new InputStream() {
			@Override
			public int read() throws IOException {
				return 150;
			}
		});
		
		TwitterTrackApiCaller twitterTrackApiMessageRetrieverImpl = new TwitterTrackApiCaller(twitterAuthenticatedHttpMethodCallerMock);
		TwitterMessageReader reader = twitterTrackApiMessageRetrieverImpl.getMessagesFromTwitter();
		assertNotNull(reader);
	}
}
