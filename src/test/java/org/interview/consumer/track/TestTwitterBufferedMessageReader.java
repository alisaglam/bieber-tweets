package org.interview.consumer.track;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.BufferedReader;
import java.io.IOException;

import org.junit.Test;
import org.mockito.Mockito;

public class TestTwitterBufferedMessageReader {

	@Test
	public void testNullBufferedReader() {
		TwitterBufferedMessageReader twitterBufferedMessageReader = new TwitterBufferedMessageReader(null);
		String message = twitterBufferedMessageReader.readMessage();
		assertNull(message);
	}
	
	@Test
	public void testNonNullBufferedReader() throws IOException {
		BufferedReader bufferedReaderMock = Mockito.mock(BufferedReader.class);
		
		Mockito.when(bufferedReaderMock.readLine()).thenReturn("test_bufferedReader");
		
		TwitterBufferedMessageReader twitterBufferedMessageReader = new TwitterBufferedMessageReader(bufferedReaderMock);
		String message = twitterBufferedMessageReader.readMessage();
		assertEquals("test_bufferedReader", message);
	}
}
