package org.interview;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.interview.common.AbstractTask;
import org.interview.common.ApplicationProperties;
import org.interview.common.file.FileWriter;
import org.interview.common.file.FileWriterImpl;
import org.interview.common.http.TwitterAuthenticatedHttpMethodCaller;
import org.interview.common.json.JsonObjectMapperImpl;
import org.interview.common.oauth.twitter.TwitterAuthenticationException;
import org.interview.common.oauth.twitter.TwitterAuthenticator;
import org.interview.common.queue.InMemoryTwitterMessageQueue;
import org.interview.common.queue.TwitterMessageQueue;
import org.interview.common.repository.InMemoryTwitterMessageRepository;
import org.interview.common.repository.TwitterMessageRepository;
import org.interview.consumer.TwitterMessageConsumer;
import org.interview.consumer.track.TwitterApiCaller;
import org.interview.consumer.track.TwitterTrackApiCaller;
import org.interview.messageparser.TwitterMessageParser;
import org.interview.statistics.FileStatisticsRepository;
import org.interview.statistics.PerSecodsStatisticsWriter;
import org.interview.statistics.StatisticsRepository;
import org.interview.writer.OutputProvider;
import org.interview.writer.StdoutOutputProvider;
import org.interview.writer.TwitterMessageWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {
	static Logger logger = LoggerFactory.getLogger(Application.class);

	/**
	 * Main Method
	 * @param args no args provided
	 */
	public static void main(String[] args) {
		TwitterAuthenticatedHttpMethodCaller twitterAuthenticatedHttpMethodCaller = authenticate();
		if(twitterAuthenticatedHttpMethodCaller != null) {
			JsonObjectMapperImpl jsonObjectMapper = new JsonObjectMapperImpl();
			TwitterMessageRepository twitterMessageRepository = new InMemoryTwitterMessageRepository();
			
			ExecutorService twitterMessageRetrieverExecutor = runMessageConsumeAndParseThreads(twitterAuthenticatedHttpMethodCaller, jsonObjectMapper, twitterMessageRepository);
			waitWhileAllThreadsInExecutorServiceNotFinished(twitterMessageRetrieverExecutor);
			
			ExecutorService outputExecutorService = runMessageOutputAndStatisticsThreads(jsonObjectMapper, twitterMessageRepository);
			outputExecutorService.shutdown();
		}
	}

	/**
	 * Creates TwitterMessageConsumer and TwitterMessageParser threads and run them
	 * 
	 * @param twitterAuthenticatedHttpMethodCaller authenticated twitter http method caller
	 * @param jsonObjectMapper json processor
	 * @param twitterMessageRepository repository used to keep twitter messages
	 * @return ExecutorService which is used to execute the two threads
	 */
	private static ExecutorService runMessageConsumeAndParseThreads(TwitterAuthenticatedHttpMethodCaller twitterAuthenticatedHttpMethodCaller,
			JsonObjectMapperImpl jsonObjectMapper, TwitterMessageRepository twitterMessageRepository) {
		TwitterMessageQueue<String> messageConsumeQueue = new InMemoryTwitterMessageQueue<String>();
		TwitterMessageConsumer retrieveTrackFromTwitter = createTwitterMessageConsumer(twitterAuthenticatedHttpMethodCaller, messageConsumeQueue);
		TwitterMessageParser twitterMessageParser = new TwitterMessageParser(messageConsumeQueue, jsonObjectMapper, twitterMessageRepository);

		ExecutorService twitterMessageRetrieverExecutor = runThreadsInExecutorService(retrieveTrackFromTwitter,twitterMessageParser);
		
		return twitterMessageRetrieverExecutor;
	}

	
	/**
	 * Creates TwitterMessageWriter and PerSecondsStatisticsWriter threads and run them
	 * @param jsonObjectMapper json processor
	 * @param twitterMessageRepository repository used to keep twitter messages
	 * @return ExecutorService which is used to execute the two threads
	 */
	private static ExecutorService runMessageOutputAndStatisticsThreads(JsonObjectMapperImpl jsonObjectMapper,
			TwitterMessageRepository twitterMessageRepository) {
		OutputProvider stdoutOutputProvider = new StdoutOutputProvider();
		TwitterMessageWriter twitterMessageWriter = new TwitterMessageWriter(jsonObjectMapper, twitterMessageRepository, stdoutOutputProvider);
		PerSecodsStatisticsWriter statisticsPerSecodsWriter = createPerSecodsStatisticsWriter(jsonObjectMapper, twitterMessageRepository);
		
		ExecutorService outputExecutorService = runThreadsInExecutorService(twitterMessageWriter, statisticsPerSecodsWriter);
		return outputExecutorService;
	}


	/**
	 * Creates PerSecondStatisticsWriter Thread
	 * @param jsonObjectMapper json processor
	 * @param twitterMessageRepository epository used to keep twitter messages
	 * @return the thread
	 */
	private static PerSecodsStatisticsWriter createPerSecodsStatisticsWriter(JsonObjectMapperImpl jsonObjectMapper,
			TwitterMessageRepository twitterMessageRepository) {
		FileWriter fileWriter = new FileWriterImpl();
		StatisticsRepository statisticsRepository = new FileStatisticsRepository(jsonObjectMapper, fileWriter);
		PerSecodsStatisticsWriter statisticsPerSecodsWriter = new PerSecodsStatisticsWriter(twitterMessageRepository, statisticsRepository);
		return statisticsPerSecodsWriter;
	}

	/**
	 * Creates TwitterMessageConsumer thread
	 * 
	 * @param twitterAuthenticatedHttpMethodCaller authenticated http method caller
	 * @param messageConsumeQueue queue which is used to transport messages between threads
	 * @return TwitterMessageConsumer thread
	 */
	private static TwitterMessageConsumer createTwitterMessageConsumer(TwitterAuthenticatedHttpMethodCaller twitterAuthenticatedHttpMethodCaller, TwitterMessageQueue<String> messageConsumeQueue) {
		int maxMessageSize = ApplicationProperties.getInstance().getMaxMessageSize();
		long maxDurationInMilis = ApplicationProperties.getInstance().getMaxDurationInMilis();
		TwitterApiCaller twitterTrackApiMessageRetriever = new TwitterTrackApiCaller(twitterAuthenticatedHttpMethodCaller);
		TwitterMessageConsumer retrieveTrackFromTwitter = new TwitterMessageConsumer(messageConsumeQueue, twitterTrackApiMessageRetriever, maxMessageSize, maxDurationInMilis);
		return retrieveTrackFromTwitter;
	}
	
	/**
	 * Creates ExecutorService of which thread pool is equal to thread size and executes the threads in the ExecutorServices
	 * @param tasks threads to be executed
	 * @return The ExecutorService where the threads run 
	 */
	private static ExecutorService runThreadsInExecutorService(AbstractTask...tasks) {
		ExecutorService executorService = Executors.newFixedThreadPool(tasks.length);
		for (AbstractTask task : tasks) {
			executorService.execute(task);
		}
		return executorService;
	}

	/**
	 * Waits main thread up to all threads in executor service finished their jobs. 
	 * @param executorService where threads are running
	 */
	private static void waitWhileAllThreadsInExecutorServiceNotFinished(ExecutorService executorService) {
		executorService.shutdown();
		while (!executorService.isTerminated()) {}
	}

	/**
	 * Authenticates user to twitter api
	 * @return Authenticated twitter http method caller
	 */
	private static TwitterAuthenticatedHttpMethodCaller authenticate() {
		try {
			ApplicationProperties applicationProperties = ApplicationProperties.getInstance();
			String consumerKey = applicationProperties.getPropery(ApplicationProperties.CONSUMER_KEY);
			String consumerSecret = applicationProperties.getPropery(ApplicationProperties.CONSUMER_SECRET);
			TwitterAuthenticator twitterAuthenticator = new TwitterAuthenticator(System.out, consumerKey,consumerSecret);
			return twitterAuthenticator.getAuthorizedHttpRequestFactory();
		} catch (TwitterAuthenticationException e) {
			logger.error("Twitter Authentication Exception.", e);
		}
		return null;
	}
}
