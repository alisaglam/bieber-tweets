package org.interview.consumer;

import org.interview.common.AbstractTask;
import org.interview.common.queue.TwitterMessageQueue;
import org.interview.consumer.track.TwitterApiCaller;
import org.interview.consumer.track.TwitterMessageReader;

/**
 * Thread used to consume twitter track api for a specific duration or up to max message count.
 * 
 * @author alis
 *
 */
public class TwitterMessageConsumer extends AbstractTask {
	private TwitterMessageQueue<String> messageQueue;
	private TwitterApiCaller twitterTrackApiMessageRetriever;
	private int maxMessageSize;
	private long maxDurationInMilis;
	private int numberOfSentMessages = 0;

	/**
	 * Constructor
	 * @param messageQueue retrieved messages send to this message queue
	 * @param twitterTrackApiMessageRetriever used make api call to twitter
	 * @param maxMessageSize maximum message count to be retrieved
	 * @param maxDurationInMilis maximum duration to retrieve messages from twitter
	 */
	public TwitterMessageConsumer(TwitterMessageQueue<String> messageQueue, TwitterApiCaller twitterTrackApiMessageRetriever, int maxMessageSize, long maxDurationInMilis) {
		this.messageQueue = messageQueue;
		this.twitterTrackApiMessageRetriever = twitterTrackApiMessageRetriever;
		this.maxMessageSize = maxMessageSize;
		this.maxDurationInMilis = maxDurationInMilis;
	}

	@Override
	protected void runTask() {
		long consumeStartTimeInMilis = System.currentTimeMillis();
		while (notReachedToMaxMessageAndNoTimeout(consumeStartTimeInMilis, maxDurationInMilis, maxMessageSize)) {
			TwitterMessageReader twitterMessageReader = twitterTrackApiMessageRetriever.getMessagesFromTwitter();
			String message;
			while (twitterMessageReader != null && (message = twitterMessageReader.readMessage()) != null 
					&& notReachedToMaxMessageAndNoTimeout(consumeStartTimeInMilis, maxDurationInMilis, maxMessageSize)) {
				messageQueue.send(message);
				numberOfSentMessages++;
			}
		}
		messageQueue.send(TwitterMessageQueue.NO_MORE_MESSAGE);
	}

	/**
	 * Check whether stop to retrieve messages or not
	 * 
	 * @param startTimeInMilis time that we started to consume twitter api
	 * @param maxDurationInMilis max duration for consuming
	 * @param maxMessageSize max number of messages to retrieve
	 * @return stop or continue
	 */
	private boolean notReachedToMaxMessageAndNoTimeout(long startTimeInMilis, long maxDurationInMilis, int maxMessageSize) {
		return checkNoTimeout(startTimeInMilis, maxDurationInMilis) && checkNotReachedToMessageLimit(maxMessageSize);
	}

	/**
	 * check whether we reached to max message count or not
	 * @param maxMessageSize max message count
	 * @return we reached or not
	 */
	private boolean checkNotReachedToMessageLimit(int maxMessageSize) {
		return numberOfSentMessages < maxMessageSize;
	}

	/**
	 * check timeout occured or not
	 * @param startTimeInMilis time that we started to consume
	 * @param maxDurationInMilis maximum duration
	 * @return timeout or not
	 */
	private boolean checkNoTimeout(long startTimeInMilis, long maxDurationInMilis) {
		return System.currentTimeMillis() - startTimeInMilis < maxDurationInMilis;
	}
}