package org.interview.consumer.track;

/**
 * Interface used to call twitter dev api
 * 
 * @author alis
 *
 */
public interface TwitterApiCaller {
	/**
	 * Used to retrieve messages from twitter api
	 * @return interface used to read retrieved messages from twitter
	 */
	TwitterMessageReader getMessagesFromTwitter();
}
