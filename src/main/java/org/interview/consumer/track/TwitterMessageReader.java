package org.interview.consumer.track;

/**
 * Interface used to read messages retrieved from twitter dev api
 * 
 * @author alis
 *
 */
public interface TwitterMessageReader {

	/**
	 * Read retrieved messages 
	 * 
	 * @return message texts
	 */
	String readMessage();

}
