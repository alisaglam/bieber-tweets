package org.interview.consumer.track;

import java.io.BufferedReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Buffered message reader implementation of TwitterMessageReader interface
 * @author alis
 *
 */
public class TwitterBufferedMessageReader implements TwitterMessageReader {
	Logger logger = LoggerFactory.getLogger(TwitterBufferedMessageReader.class);
	private BufferedReader bufferedReader;

	/**
	 * Constructor
	 * @param bufferedReader instance used to read messages in an efficient way
	 */
	public TwitterBufferedMessageReader(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}
	
	@Override
	public String readMessage() {
		if(bufferedReader != null) {
			try {
				return bufferedReader.readLine();
			} catch (IOException e) {
				logger.error("IO Exception while reading from Buffered Reader!", e);
			}
		}
		return null;
	}
}
