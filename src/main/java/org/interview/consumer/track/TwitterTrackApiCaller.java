package org.interview.consumer.track;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.interview.common.ApplicationProperties;
import org.interview.common.http.TwitterAuthenticatedHttpMethodCaller;
import org.interview.common.http.TwitterHttpRequest;
import org.interview.common.http.TwitterHttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of TwitterApiCaller interface used to make api call to
 * twitter track method
 * 
 * @author alis
 *
 */
public class TwitterTrackApiCaller implements TwitterApiCaller {
	Logger logger = LoggerFactory.getLogger(TwitterTrackApiCaller.class);
	private TwitterAuthenticatedHttpMethodCaller twitterAuthenticatedHttpMethodCaller;
	
	
	/**
	 *  Constructor
	 * @param twitterAuthenticatedHttpMethodCaller instance used to make http request to twitter api
	 */
	public TwitterTrackApiCaller(TwitterAuthenticatedHttpMethodCaller twitterAuthenticatedHttpMethodCaller) {
		this.twitterAuthenticatedHttpMethodCaller = twitterAuthenticatedHttpMethodCaller;
	}

	@Override
	public TwitterMessageReader getMessagesFromTwitter() {
		try {
			TwitterHttpRequest request = twitterAuthenticatedHttpMethodCaller.buildGetRequest(getTrackApiUrlForPhrase());
			TwitterHttpResponse response = request.execute();
			if(response != null && response.getContent() != null) {
				InputStreamReader reader = new InputStreamReader(response.getContent());
				BufferedReader in = new BufferedReader(reader);
				return new TwitterBufferedMessageReader(in);
			}
		} catch (IOException e) {
			logger.error("IO Exception while retrieving messages from twitter!", e);
		}
		return null;
	}
	
	private static String getTrackApiUrlForPhrase() {
		String twitterTrackApiUrl = ApplicationProperties.getInstance().getPropery(ApplicationProperties.TWITTER_TRACK_API_URL);
		String phrase = ApplicationProperties.getInstance().getPropery(ApplicationProperties.PHRASE);
		return twitterTrackApiUrl + phrase;
	}
	
}
