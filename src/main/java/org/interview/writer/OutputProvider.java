package org.interview.writer;

public interface OutputProvider {

	void provideOutput(String result);
}
