package org.interview.writer;

import java.util.List;

import org.interview.common.entity.Author;
import org.interview.common.entity.Message;

/**
 * Retrieved twitter messages are groped by author in this class. Comparable interface used to 
 * sort messages by author
 * 
 * @author alis
 *
 */
public class GroupedMessage implements Comparable<GroupedMessage>{
	private Author author;
	private List<Message> messages;
	
	/**
	 * Constructor 
	 * 
	 * @param author Writer of the messages
	 * @param messages list of the messages written by the author
	 */
	public GroupedMessage(Author author, List<Message> messages) {
		this.author = author;
		this.messages = messages;
	}

	public Author getAuthor() {
		return author;
	}
	
	public List<Message> getMessages() {
		return messages;
	}
	
	@Override
	public int hashCode() {
		return author.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof GroupedMessage) {
			return author.equals(((GroupedMessage) obj).getAuthor());
		}
		return false;
	}
	
	@Override
	public int compareTo(GroupedMessage o) {
		return author.compareTo(o.getAuthor());
	}
}
