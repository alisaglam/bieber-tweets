package org.interview.writer;

public class StdoutOutputProvider implements OutputProvider {

	@Override
	public void provideOutput(String result) {
		System.out.println(result);
	}
}
