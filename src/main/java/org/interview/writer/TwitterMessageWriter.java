package org.interview.writer;

import java.util.ArrayList;
import java.util.List;

import org.interview.common.AbstractTask;
import org.interview.common.entity.Author;
import org.interview.common.entity.Message;
import org.interview.common.json.JsonException;
import org.interview.common.json.JsonObjectMapper;
import org.interview.common.repository.TwitterMessageRepository;
import org.interview.messageparser.TwitterMessageParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Thread used to write grouped and sorted messages to console in json which is easily processable format 
 * 
 * @author alis
 *
 */
public class TwitterMessageWriter extends AbstractTask {
	Logger logger = LoggerFactory.getLogger(TwitterMessageParser.class);
	
	private JsonObjectMapper jsonObjectMapper;
	private TwitterMessageRepository twitterMessageRepository;
	private OutputProvider outputProvider;
	
	/**
	 * Constructor 
	 * 
	 * @param jsonObjectMapper json serializer and deserializer
	 * @param twitterMessageRepository repository which is used to get retrieved messages from twitter
	 */
	public TwitterMessageWriter(JsonObjectMapper jsonObjectMapper, TwitterMessageRepository twitterMessageRepository
			, OutputProvider outputProvider) {
		this.jsonObjectMapper = jsonObjectMapper;
		this.twitterMessageRepository = twitterMessageRepository;
		this.outputProvider = outputProvider;
	}

	@Override
	public void runTask() {
		try {
			List<GroupedMessage> groupedMessageList = getGroupedMessages();
			String output = jsonObjectMapper.serializeObjectToJson(groupedMessageList);
			if(output != null) {
				outputProvider.provideOutput(output);
			} else {
				logger.info("No message retrieved from twitter!");
			}
		} catch (JsonException e) {
			logger.error("Error while serialization of the grouped messages to json", e);
		}
		
	}

	/**
	 * Creates grouped and sorted messages 
	 * @return
	 */
	private List<GroupedMessage> getGroupedMessages() {
		List<Author> authors = twitterMessageRepository.getDistinctAuthorsInChronologicallyAscOrder();
		List<GroupedMessage> groupedMessageList = new ArrayList<GroupedMessage>();
		for (Author author : authors) {
			List<Message> messages = twitterMessageRepository.getChronologicallySortedMessagesOfTheAuthor(author);
			groupedMessageList.add(new GroupedMessage(author, messages));
			
		}
		return groupedMessageList;
	}
}
