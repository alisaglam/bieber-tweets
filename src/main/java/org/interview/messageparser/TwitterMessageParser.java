package org.interview.messageparser;

import org.interview.common.AbstractTask;
import org.interview.common.entity.Message;
import org.interview.common.json.JsonException;
import org.interview.common.json.JsonObjectMapper;
import org.interview.common.queue.TwitterMessageQueue;
import org.interview.common.repository.TwitterMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Thread used to parse json which is retrieved from twitter api and keep them on Twitter Message Repository
 * 
 * @author alis
 *
 */
public class TwitterMessageParser extends AbstractTask {
	Logger logger = LoggerFactory.getLogger(TwitterMessageParser.class);
	
	private TwitterMessageQueue<String> messageConsumeQueue;
	private JsonObjectMapper objectMapper;
	private int messageConsumeQueuePollIndex = 0;
	private TwitterMessageRepository twitterMessageRepository;

	/**
	 * Constructor
	 * @param messageConsumeQueue Message Queue used to get twitter messages in json format which are already retrieved from twitter 
	 * @param objectMapper used to deserialize json to object
	 * @param twitterMessageRepository Repository used to keep twitter messages
	 */
	public TwitterMessageParser(TwitterMessageQueue<String> messageConsumeQueue, JsonObjectMapper objectMapper, TwitterMessageRepository twitterMessageRepository) {
		this.messageConsumeQueue = messageConsumeQueue;
		this.objectMapper = objectMapper;
		this.twitterMessageRepository = twitterMessageRepository;
	}

	@Override
	protected void runTask() {
		while(true) {
			String twitterMessage = messageConsumeQueue.poll(messageConsumeQueuePollIndex);
			if(TwitterMessageQueue.NO_MORE_MESSAGE.equals(twitterMessage)) {
				break;
			}
			if(twitterMessage != null) {
				messageConsumeQueuePollIndex++;
				Message message;
				try {
					message = objectMapper.deserializeJson(twitterMessage, Message.class);
					if(message != null) {
						twitterMessageRepository.addMessage(message);
					}
				} 
				catch (JsonException e) {
					logger.error("Error while deserialization of json message: " + twitterMessage, e);
				}
			} 
		}
	}
}
