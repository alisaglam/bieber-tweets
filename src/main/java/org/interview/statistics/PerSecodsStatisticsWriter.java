package org.interview.statistics;

import java.util.Map;

import org.interview.common.AbstractTask;
import org.interview.common.repository.TwitterMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Thread used to construct statistics from messages and keep them on statistics repository 
 * 
 * @author alis
 *
 */
public class PerSecodsStatisticsWriter extends AbstractTask {
	Logger logger = LoggerFactory.getLogger(PerSecodsStatisticsWriter.class);

	private TwitterMessageRepository twitterMessageRepository;
	private StatisticsRepository statisticsRepository;

	/**
	 * Constructor 
	 * 
	 * @param twitterMessageRepository repository of messages which are retrieved from twitter
	 * @param statisticsRepository repository which are used to keep statistics
	 */
	public PerSecodsStatisticsWriter(TwitterMessageRepository twitterMessageRepository, 
			StatisticsRepository statisticsRepository) {
		this.twitterMessageRepository = twitterMessageRepository;
		this.statisticsRepository = statisticsRepository;
	}

	@Override
	protected void runTask() {
		Map<Long, Integer> messageCountGroupedByTime = twitterMessageRepository.getMessageCountGroupedByTime();
		statisticsRepository.addPerSecondStatistics(messageCountGroupedByTime);
	}
}
