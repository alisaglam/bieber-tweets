package org.interview.statistics;

import java.io.File;
import java.util.Map;

import org.interview.common.file.FileWriter;
import org.interview.common.json.JsonException;
import org.interview.common.json.JsonObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of Statistics repository which uses file for persistent stores
 * 
 * @author alis
 *
 */
public class FileStatisticsRepository implements StatisticsRepository {
	Logger logger = LoggerFactory.getLogger(FileStatisticsRepository.class);
	private static final String STATISTICS_FILE_NAME = "statistics.txt";
	
	private JsonObjectMapper jsonObjectMapper;
	private FileWriter fileWriter;
	
	/**
	 * Constructor
	 * 
	 * @param jsonObjectMapper instance used for json processing
	 * @param fileWriter instance used to write to file
	 */
	public FileStatisticsRepository(JsonObjectMapper jsonObjectMapper, FileWriter fileWriter) {
		this.jsonObjectMapper = jsonObjectMapper;
		this.fileWriter = fileWriter;
	}

	@Override
	public void addPerSecondStatistics(Map<Long, Integer> statistics) {
		try {
			String statisticsJson = jsonObjectMapper.serializeObjectToJson(statistics);
			File file = new File(STATISTICS_FILE_NAME);
			fileWriter.writeStringToFile(file.getPath(), statisticsJson);
		} catch (JsonException e) {
			logger.error("Error while serialization of per second statistics", e);
		}
	}
	
}