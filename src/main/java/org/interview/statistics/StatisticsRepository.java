package org.interview.statistics;

import java.util.Map;

/**
 * Repository used to keep statistics of messages
 *  
 * @author alis
 *
 */
public interface StatisticsRepository {
	/**
	 * Persist per second statistics of the messages retrieved 
	 * 
	 * @param statistics per secod statistics
	 */
	void addPerSecondStatistics(Map<Long, Integer> statistics);
}
