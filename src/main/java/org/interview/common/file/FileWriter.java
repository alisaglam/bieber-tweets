package org.interview.common.file;

public interface FileWriter {
	void writeStringToFile(String filePath, String value);
}
