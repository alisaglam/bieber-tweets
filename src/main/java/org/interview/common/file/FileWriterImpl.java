package org.interview.common.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileWriterImpl implements FileWriter {
	Logger logger = LoggerFactory.getLogger(FileWriterImpl.class);

	@Override
	public void writeStringToFile(String filePath, String value) {
		if(value == null) {
			return;
		}
		try {
			byte[] content = new StringBuffer(value).append(System.lineSeparator()).toString().getBytes();
			Files.write(Paths.get(filePath), content, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			logger.error("Error while writing statistics to file!", e);
		}
	}

}
