package org.interview.common.json;

/**
 * Used when exception occured while json processing (serialization or deserialization)
 * @author alis
 *
 */
public class JsonException extends Exception {
	private static final long serialVersionUID = 2746715276003169726L;

	public JsonException() {
		super();
	}
	
	public JsonException(final String message) {
		super(message);
	}
	
	public JsonException(final String message, final Throwable t) {
		super(message, t);
	}
	
	public JsonException(final Throwable t) {
		super(t);
	}

}
