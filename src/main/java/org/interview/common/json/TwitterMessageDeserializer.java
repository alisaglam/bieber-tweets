package org.interview.common.json;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.deser.std.StdDeserializer;
import org.interview.common.entity.Author;
import org.interview.common.entity.Message;

/**
 * Custom deserializer used to deserialize twitter json message to get only required fields and construct Message 
 * entity with these fields
 * 
 * @author alis
 *
 */
public class TwitterMessageDeserializer extends StdDeserializer<Message> {

	/**
	 * Default Constructor
	 */
	public TwitterMessageDeserializer() {
		this(null);
	}

	/**
	 * Constructor 
	 * @param vc Value Class
	 */
	protected TwitterMessageDeserializer(Class<?> vc) {
		super(vc);
	}

	/**
	 * Constructs Message Entity
	 */
	@Override
	public Message deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		Long id = Long.valueOf(node.get("id_str").asText());
		Long creationTimeInEpochSeconds = parseDateValueToEpochSeconds(node.get("created_at"));
		String messageText = node.get("text").asText();

		JsonNode userNode = node.get("user");
		Author author = deserializeAuthor(userNode);
		return new Message(id, creationTimeInEpochSeconds, messageText, author);
	}

	/**
	 * Constructs Author entity from the json 
	 * 
	 * @param userNode json node which contains author information
	 * @return Author instance
	 */
	private Author deserializeAuthor(JsonNode userNode) {
		Long userId = Long.valueOf(userNode.get("id_str").asText());
		String userName = userNode.get("name").asText();
		String userScreenName = userNode.get("screen_name").asText();
		Long userCreationTimeInEpochSeconds = parseDateValueToEpochSeconds(userNode.get("created_at"));
		Author author = new Author(userId, userCreationTimeInEpochSeconds, userName, userScreenName);
		return author;
	}

	/**
	 * parse twitter's date json node to epoch value
	 * 
	 * @param creationTimeNode json node that contains date information
	 * @return time in epoch format
	 */
	private long parseDateValueToEpochSeconds(JsonNode creationTimeNode) {
		String creationTimeString = creationTimeNode.asText();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss Z yyyy");
		long creationTimeInEpochSeconds = LocalDateTime.parse(creationTimeString, dateTimeFormatter).atZone(ZoneId.of("GMT")).toInstant().toEpochMilli() / 1000;
		return creationTimeInEpochSeconds;
	}
}
