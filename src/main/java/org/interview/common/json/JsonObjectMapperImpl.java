package org.interview.common.json;

import java.io.IOException;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.module.SimpleModule;
import org.interview.common.entity.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * jackson (http://wiki.fasterxml.com/JacksonHome) implementation of JsonObjectMapper
 * 
 * @author alis
 *
 */
public class JsonObjectMapperImpl implements JsonObjectMapper {
	Logger logger = LoggerFactory.getLogger(JsonObjectMapperImpl.class);
	private ObjectMapper objectMapper;

	/**
	 * In this constructor a module registered to ObjectMapper to add custom serializer and deserializer
	 */
	public JsonObjectMapperImpl() {
		objectMapper = new ObjectMapper();
		SimpleModule testModule = new SimpleModule("TwitterMessageModude", new Version(1, 0, 0, null))
				.addDeserializer(Message.class, new TwitterMessageDeserializer())
				.addSerializer(Message.class, new TwitterMessageSerializer());
		objectMapper.registerModule(testModule);
	}

	@Override
	public <T> T deserializeJson(String json, Class<T> clazz) throws JsonException {
		try {
			return objectMapper.readValue(json, clazz);
		} catch (IOException e) {
			throw new JsonException("Error while serialization of object to json" , e);
		}
	}
	
	@Override
	public String serializeObjectToJson(Object object) throws JsonException {
		try {
			return objectMapper.writeValueAsString(object);
		} catch (IOException e) {
			throw new JsonException("Error while serialization of object to json" , e);
		}
	}
}
