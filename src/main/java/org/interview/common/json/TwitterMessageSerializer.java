package org.interview.common.json;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.interview.common.entity.Message;

/**
 * Custom serializer which is used to serialize Message entity to json. We don't want author information
 * when we serialize message entity. The class used to discard author when serializing Messsage instances
 * 
 * @author alis
 *
 */
public class TwitterMessageSerializer extends JsonSerializer<Message> {

	@Override
	public void serialize(Message value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeNumberField("id", value.getId());
		jgen.writeNumberField("creationTimeInEpochSecods", value.getCreationTimeInEpochSeconds());
		jgen.writeStringField("text", value.getText());
		jgen.writeEndObject();

	}
}
