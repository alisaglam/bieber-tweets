package org.interview.common.json;

/**
 * This interface used for deserialization from json string or seralization to json.
 * 
 * @author alis
 *
 */
public interface JsonObjectMapper {
	/**
	 * Deserialize json string to object
	 * 
	 * @param json json string
	 * @param clazz desiralized object class
	 * @return deserialized object
	 * @throws JsonException when any exception occurs while deserializing 
	 */
	public <T> T deserializeJson(String json, Class<T> clazz) throws JsonException;
	
	/**
	 * Serialize object to json
	 * 
	 * @param object serialized object
	 * @return json string
	 * @throws JsonException when any exception occurs while serializing
	 */
	public String serializeObjectToJson(Object object) throws JsonException;
}
