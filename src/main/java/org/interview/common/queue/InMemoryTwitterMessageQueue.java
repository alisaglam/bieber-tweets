package org.interview.common.queue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple in memory message queue implementation
 * @author alis
 *
 * @param <T>
 */
public class InMemoryTwitterMessageQueue<T extends Serializable> implements TwitterMessageQueue<T>{
	private List<T> messageList;

	public InMemoryTwitterMessageQueue() {
		messageList = new ArrayList<T>();
	}
	
	@Override
	public synchronized void send(T result) {
		messageList.add(result);
	}
	
	private int getMessageSize() {
		return messageList.size();
	}
	
	@Override
	public synchronized T poll(int index) {
		if(getMessageSize() > index) {
			return messageList.get(index);
		}
		return null;
	}
}
