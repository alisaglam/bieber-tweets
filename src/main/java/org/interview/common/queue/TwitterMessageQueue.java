package org.interview.common.queue;

import java.io.Serializable;

/**
 * Message gueue interface used to send messages between different code modules. A message is the data 
 * transported between the sender and receivers. 
 * 
 * @author alis
 *
 * @param <T> Type of the message
 */
public interface TwitterMessageQueue<T extends Serializable> {
	public final static String NO_MORE_MESSAGE = "NO_MORE_MESSAGE";

	/**
	 * Send message to queue 
	 * @param message the enqueued message
	 */
	void send(T message);
	
	/**
	 * Retrieve message at the index from the queue. There might be more than one receivers, so poll indexes are
	 * managed by receivers and retrieved in this message.
	 * 
	 * @param index message index to be polled
	 * @return message
	 */
	T poll(int index);
}
