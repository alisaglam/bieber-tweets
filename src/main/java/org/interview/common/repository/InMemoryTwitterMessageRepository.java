package org.interview.common.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.interview.common.entity.Author;
import org.interview.common.entity.Message;

public class InMemoryTwitterMessageRepository implements TwitterMessageRepository {
	private List<Message> messageList;
	
	public InMemoryTwitterMessageRepository() {
		messageList = new ArrayList<Message>();
	}

	@Override
	public void addMessage(Message message) {
		messageList.add(message);
	}
	
	@Override
	public List<Author> getDistinctAuthorsInChronologicallyAscOrder() {
		List<Author> resultList = new ArrayList<Author>();
		for (Message message : messageList) {
			Author author = message.getAuthor();
			if(resultList.contains(author) == false) {
				resultList.add(author);
			}
		}
		Collections.sort(resultList);
		return resultList;
	}
	
	@Override
	public List<Message> getChronologicallySortedMessagesOfTheAuthor(Author author) {
		List<Message> resultList = new ArrayList<Message>();
		if(author == null) {
			return resultList;
		}
		for (Message message : messageList) {
			if(message.getAuthor().equals(author)) {
				resultList.add(message);
			}
		}
		return resultList;
	}
	
	@Override
	public Map<Long, Integer> getMessageCountGroupedByTime() {
		Map<Long, Integer> messageCountGroupedByTime = new HashMap<Long, Integer>();
		for (Message message : messageList) {
			Long creationTimeInSecods = Long.valueOf(message.getCreationTimeInEpochSeconds());
			if(messageCountGroupedByTime.containsKey(creationTimeInSecods) == false) {
				messageCountGroupedByTime.put(creationTimeInSecods, Integer.valueOf(0));
			}
			int count = messageCountGroupedByTime.get(creationTimeInSecods).intValue();
			messageCountGroupedByTime.put(creationTimeInSecods, count+1);
		}
		return messageCountGroupedByTime;
	}
}
