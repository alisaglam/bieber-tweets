package org.interview.common.repository;

import java.util.List;
import java.util.Map;

import org.interview.common.entity.Author;
import org.interview.common.entity.Message;

/**
 * Messages which are retrieved from twitter api are persisted and queried by this interface
 * 
 * @author alis
 *
 */
public interface TwitterMessageRepository {
	/**
	 * Add twitter message to repository
	 * @param message to be kept on the repository
	 */
	void addMessage(Message message);
	
	/**
	 * Retrieves all authors of the twitter messages in chronologically ascending order from the repository
	 * @return list of authors in the order
	 */
	List<Author> getDistinctAuthorsInChronologicallyAscOrder();
	
	/**
	 * Retrieves all messages of the author in chronologically ascending order from the repository
	 * @param author the writer of the messages
	 * @return list of messages written by the author
	 */
	List<Message> getChronologicallySortedMessagesOfTheAuthor(Author author);
	
	/**
	 * Retrieves number of messages grouped by creation times in secods.
	 * @return number of messages
	 */
	Map<Long, Integer> getMessageCountGroupedByTime();
}
