package org.interview.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Singleton class used to read application properties from resource bundle
 * 
 * @author alis
 *
 */
public class ApplicationProperties {
	private Map<String, String> properties;
	public final static String CONSUMER_KEY = "twitter.consumerkey";
	public final static String CONSUMER_SECRET = "twitter.consumersecret";
	public final static String PHRASE = "twitter.phrase";
	public final static String TWITTER_TRACK_API_URL = "twitter.trackapi.url";
	public final static String MAX_MESSAGE_SIZE = "max.message.size";
	public final static String MESSAGE_RETRIEVE_DURATION_IN_SECONDS = "message.retrieve.duration.inseconds";
	
	private static ApplicationProperties applicationProperties;
	
	/**
	 * private constructor
	 */
	private ApplicationProperties() {
		properties = new HashMap<String, String>();
	}

	/**
	 * Singleton instance created if not exist and returned 
	 * 
	 * @return singleton instance of this class
	 */
	public static ApplicationProperties getInstance() {
		if(applicationProperties == null) {
			applicationProperties = new ApplicationProperties();
			applicationProperties.loadApplicationProperties();
		}
		return applicationProperties;
	}

	/**
	 * Reads properties from the file and keeps them in memory 
	 */
	private void loadApplicationProperties() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			String filename = "application.properties";
			input = getClass().getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				throw new RuntimeException("Unable to find application.properties file");
			}
			prop.load(input);
			Enumeration<?> e = prop.propertyNames();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				String value = prop.getProperty(key);
				properties.put(key, value);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Retrieves the application property value
	 * 
	 * @param propertyKey the key of the property written in resource bundles
	 * @return value of the application property
	 */
	public String getPropery(String propertyKey) {
		return properties.get(propertyKey);
	}
	
	public long getMaxDurationInMilis() {
		String maxDurationInSeconds = getInstance()
				.getPropery(MESSAGE_RETRIEVE_DURATION_IN_SECONDS);
		long maxDurationInMilis = Long.valueOf(maxDurationInSeconds) * 1000;
		return maxDurationInMilis;
	}

	public int getMaxMessageSize() {
		String maxMessage = getInstance().getPropery(MAX_MESSAGE_SIZE);
		int maxMessageSize = Integer.valueOf(maxMessage);
		return maxMessageSize;
	}
}
