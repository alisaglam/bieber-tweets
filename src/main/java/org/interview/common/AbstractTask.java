package org.interview.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class of the thread classes in the application. Run method is encapsulated in this class
 * 
 * @author alis
 *
 */
public abstract class AbstractTask implements Runnable {
	Logger logger = LoggerFactory.getLogger(AbstractTask.class);
	
	@Override
	public final void run() {
		logger.info(getClass() + " task started");
		runTask();
		logger.info(getClass() + " task finished");
	}
	
	/**
	 * Template method which is main job of the thread
	 */
	protected abstract void runTask();
}
