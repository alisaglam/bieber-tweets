package org.interview.common.http;

import java.io.IOException;

/**
 * Http request interface which makes the code more testable
 * 
 * @author alis
 *
 */
public interface TwitterHttpRequest {

	/**
	 * calls http method
	 * @return http method call's respopnse
	 */
	TwitterHttpResponse execute() throws IOException;
}
