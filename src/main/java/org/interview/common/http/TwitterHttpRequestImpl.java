package org.interview.common.http;

import java.io.IOException;

import com.google.api.client.http.HttpRequest;


/**
 * Google's HttpRequest class wrapped in this class
 * @author alis
 *
 */
public class TwitterHttpRequestImpl implements TwitterHttpRequest {
	private HttpRequest httpRequest;

	public TwitterHttpRequestImpl(HttpRequest httpRequest) {
		this.httpRequest = httpRequest;
	}

	@Override
	public TwitterHttpResponse execute() throws IOException {
		return new TwitterHttpResponseImpl(httpRequest.execute());
	}
}
