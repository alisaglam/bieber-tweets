package org.interview.common.http;

import java.io.IOException;
import java.io.InputStream;

import com.google.api.client.http.HttpResponse;

/**
 * Google's HttpResponse class wrapped in this class
 * @author alis
 *
 */
public class TwitterHttpResponseImpl implements TwitterHttpResponse {
	private HttpResponse httpResponse;

	public TwitterHttpResponseImpl(HttpResponse httpResponse) {
		this.httpResponse = httpResponse;
	}

	@Override
	public InputStream getContent() throws IOException {
		return httpResponse.getContent();
	}
}
