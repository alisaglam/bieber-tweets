package org.interview.common.http;

import java.io.IOException;

/**
 * Authenticated Http Method Caller Interface. This interface is created to decouple the code from concrete 
 * Http Method Caller Api's and make the code more testable 
 * 
 * @author alis
 *
 */
public interface TwitterAuthenticatedHttpMethodCaller {

	/**
	 * Builds Http Get Request from the url 
	 * 
	 * @param url The url of the Http Method
	 * @return Http request instance for get request
	 */
	TwitterHttpRequest buildGetRequest(String url) throws IOException;

}
