package org.interview.common.http;

import java.io.IOException;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;

/**
 * Google's HttpRequestFactory class is wrapped in this implementation.
 * 
 * @author alis
 *
 */
public class TwitterAuthenticatedHttpRequestFactoryImpl implements TwitterAuthenticatedHttpMethodCaller {
	
	private HttpRequestFactory httpRequestFactory;

	/**
	 * Constructor
	 * 
	 * @param httpRequestFactory authenticated http request factory to make http call to twitter api
	 */
	public TwitterAuthenticatedHttpRequestFactoryImpl(HttpRequestFactory httpRequestFactory) {
		this.httpRequestFactory = httpRequestFactory;
	}

	/**
	 * Creates Get request for the given URL
	 */
	@Override
	public TwitterHttpRequest buildGetRequest(String url) throws IOException {
		return new TwitterHttpRequestImpl(httpRequestFactory.buildGetRequest(new GenericUrl(url)));
	}
}
