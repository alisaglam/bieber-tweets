package org.interview.common.http;

import java.io.IOException;
import java.io.InputStream;

/**
 * Http Response interface which makes the code more testable
 * @author alis
 *
 */
public interface TwitterHttpResponse {

	/**
	 * Retrieves content from Http response
	 * @return InputStream of the response content
	 */
	InputStream getContent() throws IOException;
}
