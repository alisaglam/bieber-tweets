package org.interview.common.entity;

import java.io.Serializable;

/**
 * Used to keep common parts of the Twitter entities
 * 
 * @author alis
 *
 */
public abstract class AbstractTwitterEntity implements Serializable, Comparable<AbstractTwitterEntity> {
	private static final long serialVersionUID = -3048912990096711563L;
	
	private long id;
	private long creationTimeInEpochSecods;
	
	/**
	 * Default Constructor
	 */
	public AbstractTwitterEntity() {
	}

	/**
	 * Constructor
	 *  
	 * @param id Id of the entity
	 * @param creationTimeInEpochSeconds Creation time of the entity
	 */
	public AbstractTwitterEntity(long id, long creationTimeInEpochSeconds) {
		this.id = id;
		this.creationTimeInEpochSecods = creationTimeInEpochSeconds;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCreationTimeInEpochSeconds() {
		return creationTimeInEpochSecods;
	}

	public void setCreationTimeInEpochSecods(long creationTimeInEpochSecods) {
		this.creationTimeInEpochSecods = creationTimeInEpochSecods;
	}

	@Override
	public int hashCode() {
		 return (int) (id ^ (id >>> 32));
	}
	
	@Override
	public boolean equals(Object otherEntity) {
		if(otherEntity instanceof AbstractTwitterEntity) {
			return ((AbstractTwitterEntity)otherEntity).getId() == this.id;
		}
		return false;
	}
	
	@Override
	public int compareTo(AbstractTwitterEntity otherEntity) {
		return (int) (creationTimeInEpochSecods - otherEntity.getCreationTimeInEpochSeconds());
	}
}
