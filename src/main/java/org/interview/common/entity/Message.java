package org.interview.common.entity;

/**
 * Twitter Message
 * 
 * @author alis
 *
 */
public class Message extends AbstractTwitterEntity{
	private static final long serialVersionUID = 8036213601897193552L;
	
	private String text;
	private Author author;
	
	/**
	 * Default Constructor
	 */
	public Message() {
	}
	
	/**
	 * Constructor
	 *  
	 * @param id Id of the Message
	 * @param creationTimeInEpochSeconds Creation time of the message
	 * @param messageText Text of the message
	 * @param author Author of the message
	 */
	public Message(long id, long creationTimeInEpochSeconds, String messageText, Author author) {
		super(id, creationTimeInEpochSeconds);
		this.text = messageText;
		this.author = author;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}
}
