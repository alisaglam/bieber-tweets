package org.interview.common.entity;

/**
 * Author of the Twitter Messages
 * 
 * @author alis
 *
 */
public class Author extends AbstractTwitterEntity {
	private static final long serialVersionUID = -3284602777119239882L;
	
	private String name;
	private String screenName;
	
	/**
	 * Default Constructor
	 */
	public Author() {
	}
	
	/**
	 * Constructor 
	 * 
	 * @param id Id Of The Author
	 * @param creationTimeInEpochSeconds Creation time of the Author
	 * @param name Name Of The Author
	 * @param screenName Screen name of the author
	 */
	public Author(long id, long creationTimeInEpochSeconds, String name, String screenName) {
		super(id, creationTimeInEpochSeconds);
		this.name = name;
		this.screenName = screenName;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

}
